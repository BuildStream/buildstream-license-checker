# BuildStream License Checker

BuildStream License Checker is a license-checking utility for BuildStream projects.

The utility will check out the sources of a given element into a temporary
folder, use license-scanning software to collect license informaiton, and then
process the results.

The utility is written as a python script, using subprocess to invoke
BuildStream functions and the license scanning software.

### Install

First, ensure that BuildStream and the license scanning software are installed.
(See below for details of the license scanning software).

Buildstream License Checker is a python package, and can be installed with pip. Navigate
to the BuildStream License Checker's top level directory (the directory that contains
`setup.py`), and run the command
```
pip3 install .
```

### License scanning software

The license scanning software for this tool can either be
[`ScanCode`](https://pypi.org/project/scancode-toolkit), or
[`licensecheck`](https://metacpan.org/pod/distribution/App-Licensecheck/bin/licensecheck).

The tool in used can be overriden with the `--checkertool` parameter. By
default, `licensecheck` is used.

For ScanCode, run the following command before running the license checker
(example is on Fedora):

```shell
sudo dnf install scancode-toolkit
```

For licensecheck, run the following command before running the license checker
(example is on Fedora):


```shell
sudo dnf install licensecheck
```

### Usage

```
usage: bst_license_checker [-h] [-t] [-d DEPS_TYPE] [-i IGNORELIST_FILENAME] -w WORKING_DIRECTORY -o OUTPUT_DIRECTORY [-c CHECKER_TOOL] [-scp SCANCODE_PROCS] ELEMENT_NAMES [ELEMENT_NAMES ...]

A license-checking utility for buildstream projects. Takes a list of buildstream element names and uses "bst show" to generate a list of those elements' dependencies (see --deps option). Each dependency is then checked out into a
temporary folder, and scanned for license information. Results are bundled into an output directory, along with human-readable and machine-readable summary files.

positional arguments:
  ELEMENT_NAMES         One or more elements to be checked.

options:
  -h, --help            show this help message and exit
  -t, --track           Run the bst track command on each dependency before checking licenses.
  -d DEPS_TYPE, --deps DEPS_TYPE
                        The type of dependencies to scan. Will be passed directly to the 'bst show' command. Defaults to run. Choose from: none, run, all
  -i IGNORELIST_FILENAME, --ignorelist IGNORELIST_FILENAME
                        Filename for a list of elements names to ignore. Ignored elements will not be fetched, tracked or scanned for licenses. Element names in the ignore list file should be separated by line breaks (one element name
                        per line). Lines which start with a hash (#) are treated as comments.
  -w WORKING_DIRECTORY, --work WORKING_DIRECTORY
                        License results will be created here first, and saved. Can be reused (does not need to be emptied between invocations). Can be used as a cache: previously processed results will be reused if the cache key has not
                        changed.
  -o OUTPUT_DIRECTORY, --output OUTPUT_DIRECTORY
                        The path to an output directory, in which to store license results. Will be created if it doesn't already exist. Directory must be empty.
  -c CHECKER_TOOL, --checkertool CHECKER_TOOL
                        Either `licensecheck` or `scancode`, which will be explicitly used for the license scanning backend of `bst-license-checker`.
  -scp SCANCODE_PROCS, --scancode-procs SCANCODE_PROCS
                        The amount of parallel processes ScanCode should use. Default is all CPU cores. A value of `-1` disables parallel processing, and any other positive integer is the exact amount of processes to use. Only applicable
                        to ScanCode backend.
```

### How it works

When run, the script executes the following stages:

1) The script runs `bst show` to collect a list of all relevant dependencies.
The elements named in `ELEMENT_NAMES` are passed verbatim to bst show, along
with the `--deps` option. Any elements named in the `ignorelist` file are
removed from the list at this stage.

The resulting list is the full list of elements that will be scanned for
licenses. Internally, the script refers to these as "dependency elements" to
distinguish them from the intial list of elements supplied by the user as
`ELEMENT NAMES`.

2) If the `-t` option is used, the script runs the `bst track` command on all of
the dependency elements. This may change the full-key for some dependency
elements.

3) The script runs `bst fetch` on all of the dependency elements to ensure that
BuildStream has an up to date cache of all sources. This may change the status
of some dependency elements. `bst show` is run again, to get up to date values
for full-key and element statuses.

4) The script then iterates through each of the dependency elements. For each
element, the script checks out the source code of that element into a temporary
folder, and uses license scanning software to detect the licences that apply.
For each dependency element, an output file is saved to the working directory
and then copied to the output directory.

4a) Note: if the script has already scanned an element on a previous occasion,
then the output file may already exist in the working directory. If the element
name and the element full key haven't changed, then the script will not check
out or scan the element. Instead, it will simply re-use the existing result
file. In this way, the working directory can serve as a cache.

5) The script summarises the output files and produces a list of detected
licenses for each element. The results are returned in a dictionary object. The
dictionary object is used to generate a machine-readable json summary file, and a
human readable html summary file. The json format is a direct json dump of the
results, and has the following format:

```
{
  "dependency-list": [
    {
      "dependency-name": "bootstrap/acl.bst",
      "full-key": "cd819edf4915f403f9864aa6f69f5395b033ed2aed671507452e1e35783f8905",
      "checkout-status": "checkout succeeded",
      "detected-licenses": [
        "GNU General Public License v2.0 or later",
        "GNU Lesser General Public License v2.1 or later",
        "GNU Lesser General Public License, Version 2.1 GNU General Public License, Version 2"
      ],
      "output-filename": "bootstrap-acl.bst--cd819edf4915f403f9864aa6f69f5395b033ed2aed671507452e1e35783f8905.output.txt"
    },
    ...
    ...
    {
      "dependency-name": "bootstrap/attr.bst",
      "full-key": "bee8d33a7c1cd7b5b0ccbbd934bd1dfc551da5d3b6f44b1da37c06817f554790",
      "checkout-status": "checkout succeeded",
      "detected-licenses": [
        "GNU General Public License v2.0 or later",
        "GNU Lesser General Public License v2.1 or later",
        "GNU Lesser General Public License, Version 2.1 GNU General Public License, Version 2"
      ],
      "output-filename": "bootstrap-attr.bst--bee8d33a7c1cd7b5b0ccbbd934bd1dfc551da5d3b6f44b1da37c06817f554790.output.txt"
    },
    ...
    ...
  ]
}
```

### Notes

* Checking out the a source will fail if the source needs to be tracked. To proceed,
  either run the buildstream `track` command on the element manually, or use the
  `--track` option. (Note that `--track` will track every element and dependency,
  and can update existing refs.)

* Elements which have no sources (stack elements, compose elements, etc) will be
  detected as errors by the script. This happens because the script has no way of
  knowing which elements have no sources, and it encounters a BuildStream error when it
  tries to check out sources that don't exist. These errors do not cause the script to
  fail, but they can significantly slow down execution. To save time, elements that have
  no sources should be added to an ignore-list file. (See the --ignorelist option)
